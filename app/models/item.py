from app import db


class Item(db.Model):
    __tablename__ = 'New_C_I_ALIGNE'  # Specify the table name
    __table_args__ = {'schema': 'dbo'}  # Specify the schema name if needed

    I_Aligne_Id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    Legal_Entity = db.Column(db.String(50), nullable=True)

    def serialize(self):
        return {
            'I_Aligne_Id': self.I_Aligne_Id,
            'Legal_Entity': self.Legal_Entity
        }
