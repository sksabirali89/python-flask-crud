from flask_jwt_extended import jwt_required, get_jwt_identity

# Create a decorator for protected routes
def protected_route(f):
    @jwt_required()
    def wrapper(*args, **kwargs):
        current_user = get_jwt_identity()
        print(current_user)
        return f(current_user, *args, **kwargs)
    return wrapper