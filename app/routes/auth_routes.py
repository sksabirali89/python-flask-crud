from flask import request, jsonify, Blueprint
from flask_jwt_extended import create_access_token
from app import bcrypt, users
from app.routes.decorator import protected_route
auth_bp = Blueprint("auth_bp", __name__)

# Login route to get JWT token


@auth_bp.route('/api/login', methods=['POST'])
def login():
    try:
        data = request.get_json()
        username = data.get('username')
        password = data.get('password')
        password_hash = users[username]['password_hash']

        if username in users and bcrypt.check_password_hash(password_hash, password):
            access_token = create_access_token(identity=username)
            return jsonify(access_token=access_token), 200
        else:
            return jsonify(message='Invalid username or password'), 401
    except KeyError as ve:
        error_message = f"User {ve} not found!"
        return jsonify(message=error_message), 401

    except Exception as e:
        return jsonify(message='Invalid username or password'), 401


# Protected route using the decorator
@auth_bp.route('/api/protected', methods=['GET'])
@protected_route
def protected(current_user):
    return jsonify(message=f'Hello, {current_user}! This is a protected route.'), 200

# Unprotected route


@auth_bp.route('/api/unprotected', methods=['GET'])
def unprotected():
    return jsonify(message='This is an unprotected route.'), 200
