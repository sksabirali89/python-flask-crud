from flask import request, jsonify, Blueprint

from app.models.item import Item
from app import db
from app.routes.decorator import protected_route
item_bp = Blueprint("item_bp", __name__)

# Route to get all items


@item_bp.route('/api/items', methods=['GET'])
@protected_route
def get_items():
    try:
        items = Item.query.all()
        items_data = [item.serialize() for item in items]
        return jsonify(items_data), 200

    except ValueError as e:

        return jsonify({'error': str(e)}), 400
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@item_bp.route('/api/items', methods=['POST'])
def create_item():
    try:
        data = request.get_json()
        new_item = Item(Legal_Entity=data['Legal_Entity'])
        db.session.add(new_item)
        db.session.commit()
        return jsonify({'message': 'Item created successfully'}), 201
    except ValueError as e:
        return jsonify({'error': str(e)}), 400
    except Exception as e:
        return jsonify({'error': str(e)}), 500


@item_bp.route('/api/items/<int:item_id>', methods=['GET'])
def get_item(item_id):
    item = Item.query.get(item_id)
    if item:
        return jsonify({'I_Aligne_Id': item.I_Aligne_Id, 'Legal_Entity': item.Legal_Entity})
    return jsonify({'message': 'Item not found'}), 404


@item_bp.route('/api/items/<int:item_id>', methods=['PUT'])
def update_item(item_id):
    item = Item.query.get(item_id)
    if item:
        data = request.get_json()
        item.Legal_Entity = data['Legal_Entity']
        db.session.commit()
        return jsonify({'message': 'item updated successfully'})
    return jsonify({'message': 'item not found'}), 404


@item_bp.route('/api/items/<int:item_id>', methods=['DELETE'])
def delete_item(item_id):
    item = Item.query.get(item_id)
    if item:
        db.session.delete(item)
        db.session.commit()
        return jsonify({'message': 'item deleted successfully'})
    return jsonify({'message': 'item not found'}), 404
