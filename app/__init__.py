from flask import Flask
from config.config import Config
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text
from flask_jwt_extended import JWTManager
from flask_bcrypt import Bcrypt

app = Flask(__name__)

# Connect to database
app.config.from_object(Config)
db = SQLAlchemy(app)
with app.app_context():
    try:
         # Access the database using db.session
        query = text("SELECT 1")  # Declare the SQL expression as text
        db.session.execute(query)  # Execute the query to check the connection
        print("Database connection successful")
    except Exception as e:
        print(type(e).__name__)
        # print(f"Error connecting to the database: {str(e)}")

# Configure Flask app and JWT
# app.config['JWT_SECRET_KEY'] = 'your_secret_key'  # Change this to a strong, random secret key
jwt = JWTManager(app)
# Initialize Bcrypt for password hashing
bcrypt = Bcrypt(app)

# Mock user data (replace with a database)
users = {
    'user1': {
        'username': 'user1',
        'password_hash': bcrypt.generate_password_hash('password1').decode('utf-8')
    }
}


from app.routes.item_routes import item_bp
from app.routes.auth_routes import auth_bp

app.register_blueprint(item_bp)
app.register_blueprint(auth_bp)