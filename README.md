# python-flask-crud

Set up the project locally

```

git clone https://gitlab.com/sksabirali89/python-flask-crud.git
cd python-flask-crud
python -m venv venv
For Windows venv\Scripts\activate or for Mac/Linux source venv/bin/activate
pip install -r requirements.txt
python run.py

```

Freeze dependencies in the requirements.txt

```
pip freeze > requirements.txt

```
